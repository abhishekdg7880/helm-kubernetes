# Helm-Kubernetes

Made Use Of Helm to deploy the stack on the Docker-Desktop Kubernetes as Helm provides:-

1. Reusability 
2. Simplified deployments
3. Versioning
4. Templating
5. Community support

Installation for Prometheus and Grafana

1. Create a namespace in Kubernetes Cluster called monitoring like:-


        kubectl create ns monitoring

2. Install Prometheus using Helm Chart

        helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
        helm search repo prometheus-community
        helm install prometheus prometheus-community/prometheus --namespace monitoring
        kubectl get svc -n monitoring
        kubectl port-forward -n service/<prometheus-service-name> 9100:9100
        
3. Install Grafana

        create file called grafana.yml
        kubectl apply -f grafana.yml -n monitoring
        kubectl get svc -n monitoring
        kubectl port-forward service<grafana-service-name> -n monitoring 3000:3000

Troubleshooting Steps regrading Prometheus Node Exporter

Problem Statement:- node-exporter crashes at startup 


Solution:- Looked At Prometheus GitHub Repo Discussions Page

Removed the mountPropagation to use the default one.

kubectl patch ds <name-of-the-deployed-prometheus-daemonset> --type "json" -p '[{"op": "remove", "path" : "/spec/template/spec/containers/0/volumeMounts/2/mountPropagation"}]'

Attached PNG is the Output that display how the webapp pod connects to the Database running at 3306.


WebApp And MySQL Deployment

1. created Helm Charts for both App and Database
2. created namespace called app 
3. Deployed the two Helm charts on app namespace using:-
      
      helm install -f mysql/values.yaml mysql ./mysql -n app
      helm install -f webapp/values.yaml webapp ./webapp -n app

4. WebApp service Exposure :-

      kubectl get svc -n app
      kubectl port-forward -n service/<webapp-service-name> 5000:80

Improvements In the Architecture


1. Deployment on AWS will allows to make use of Ingress Controllers thereby providing the capabailities to redirect the traffic using path or host based routing mechanism.

2. Also different reverse Proxy mechanism could be included like Envoy Proxy that provides the flexibilty of mirroring traffic.

3. Network Policy could be implemented to only allow webapp pod deployments to make a communication to the mysql service.
